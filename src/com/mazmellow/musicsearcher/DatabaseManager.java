package com.mazmellow.musicsearcher;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseManager extends SQLiteOpenHelper {

	Context context;

	// Database Version
	private static final int DATABASE_VERSION = 1;

	// Database Name
	private static final String DATABASE_NAME = "musicsearcher.db";

	// Table Name
	private static final String TABLE_HISTORY = "history";

	// -------VERSION 1-------------
	// ---bookmarks---
	private static final String COL_TITLE = "title";
	// -------------------------------

	public DatabaseManager(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
		this.context = context;

	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		System.out.println("------ onCreate --------");
		try {
			db.execSQL("CREATE TABLE " + TABLE_HISTORY
					+ "("
					// HistoryID INTEGER PRIMARY KEY,"
					+ " " + COL_TITLE + " TEXT(100));");

			Log.d("CREATE TABLE", "Create Table Successfully.");
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("DB ERROR: " + e);
		}
	}

	@Override
	public void onOpen(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		super.onOpen(db);
		System.out.println("------ onOpen --------");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		System.out.println("------ onUpgrade --------");
		System.out.println("------ oldVersion : " + oldVersion);
		System.out.println("------ newVersion : " + newVersion);
	}

	// Insert Data
	public boolean insertHistory(String title) {
		// TODO Auto-generated method stub

		if (!checkHistoryByTitle(title)) {
			try {
				SQLiteDatabase db = this.getWritableDatabase();
				/**
				 * for API 11 and above SQLiteStatement insertCmd; String strSQL
				 * = "INSERT INTO " + TABLE_MEMBER +
				 * "(MemberID,Name,Tel) VALUES (?,?,?)";
				 * 
				 * insertCmd = db.compileStatement(strSQL);
				 * insertCmd.bindString(1, strMemberID); insertCmd.bindString(2,
				 * strName); insertCmd.bindString(3, strTel); return
				 * insertCmd.executeInsert();
				 */

				ContentValues Val = new ContentValues();
				Val.put(COL_TITLE, title);

				db.insert(TABLE_HISTORY, null, Val);

				if (db != null) {
					db.close();
				}

				return true; // return rows inserted.

			} catch (Exception e) {
				Log.i("insertOrUpdateHistory", e.getMessage());
				return false;
			}
		} else {
			return true;
		}

	}
	
	public boolean checkHistoryByTitle(String title) {
		// TODO Auto-generated method stub
		boolean found = false;
		try {
			// SQLiteDatabase db;
			SQLiteDatabase db = this.getWritableDatabase(); // Read Data

			String strSQL = "SELECT * FROM " + TABLE_HISTORY + " WHERE "
					+ COL_TITLE + " = '" + title + "'";
			Cursor cursor = db.rawQuery(strSQL, null);

			if (cursor != null) {
				if (cursor.moveToFirst()) {
					found = true;
				}
			}
			cursor.close();
			if (db != null) {
				db.close();
			}
			return found;

		} catch (Exception e) {
			Log.i("checkHistoryByTitle", e.getMessage());
			return found;
		}

	}

	public ArrayList<String> getAllHistory() {
		// TODO Auto-generated method stub

		try {
			ArrayList<String> list = new ArrayList<String>();

			SQLiteDatabase db = this.getWritableDatabase(); // Read Data

			String strSQL = "SELECT * FROM " + TABLE_HISTORY;
			Cursor cursor = db.rawQuery(strSQL, null);

			if (cursor != null) {
				if (cursor.moveToFirst()) {
					do {
						list.add(cursor.getString(0));
					} while (cursor.moveToNext());
				}
			}
			cursor.close();
			if (db != null) {
				db.close();
			}
			return list;

		} catch (Exception e) {
			Log.i("getAllHistory", e.getMessage());
			// System.out.println("ERROR getAllHistory: "+e);
			return null;
		}

	}

	// Delete Data
	public boolean deleteHistoryByTitle(String title) {
		// TODO Auto-generated method stub

		try {

			SQLiteDatabase db;
			db = this.getWritableDatabase(); // Write Data

			/**
			 * for API 11 and above SQLiteStatement insertCmd; String strSQL =
			 * "DELETE FROM " + TABLE_MEMBER + " WHERE MemberID = ? ";
			 * 
			 * insertCmd = db.compileStatement(strSQL); insertCmd.bindString(1,
			 * strMemberID);
			 * 
			 * return insertCmd.executeUpdateDelete();
			 * 
			 */

			db.delete(TABLE_HISTORY, COL_TITLE + " = ?",
					new String[] { String.valueOf(title) });

			db.close();
			return true; // return rows delete.

		} catch (Exception e) {
			return false;
		}

	}

}
