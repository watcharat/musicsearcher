/*******************************************************************************
 * Copyright 2011-2013 Sergey Tarasevich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.mazmellow.musicsearcher;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;

import buzzcity.android.sdk.BCAdsClientBanner;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.preference.PreferenceManager;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


public class MusicListActivity extends Activity{

	public static ArrayList<MusicObject> array;

	String[] titles;
	String[] urls;
	String[] bitrates;
	String[] durations;
	String[] filesizes;
	
	String nameVideo;
	
	public static boolean isPlay = false;
	
	final String strPref_Download_ID = "PREF_DOWNLOAD_ID_MP3SEARCHER";
//	String Download_path = "";
	String Download_ID = "DOWNLOAD_ID_MP3SEARCHER";
	SharedPreferences preferenceManager;
	DownloadManager downloadManager;
	
	ListView listView;

	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_list);

		if (array != null) {

			titles = new String[array.size()];
			urls = new String[array.size()];
			bitrates = new String[array.size()];
			durations = new String[array.size()];
			filesizes = new String[array.size()];

			for (int i = 0; i < array.size(); i++) {
				MusicObject musicObject = (MusicObject) array.get(i);
				titles[i] = musicObject.getTitle();
				urls[i] = musicObject.getUrl();
				bitrates[i] = musicObject.getBitrate();
				durations[i] = musicObject.getDuration();
				filesizes[i] = musicObject.getFilesize();
			}
		}

		listView = (ListView) findViewById(android.R.id.list);
		((ListView) listView).setAdapter(new ItemAdapter());
//		listView.setOnItemClickListener(new OnItemClickListener() {
//			
//			public void onItemClick(AdapterView<?> parent, View view,
//					int position, long id) {
//				System.out.println("vcode = " + vCodes[position]);
//				
////				downloadYoutube(vCodes[position], titles[position]);
//				
//				Intent intent = YouTubeStandalonePlayer.createVideoIntent(ImageListActivity.this, "AIzaSyBuJweYhLFmX3fvlKhWkv6lnZqNlItSlcs", vCodes[position]);
//				 startActivity(intent);
//				
////				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube://" + vCodes[position]));
////				startActivity(intent);
//				
////				startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v="+vCodes[position])));
//				
//				isPlay = true;
//			}
//		});
		
		preferenceManager = PreferenceManager.getDefaultSharedPreferences(this);
		downloadManager	= new DownloadManager(getContentResolver(), "com.mazmellow.musicsearcher");

		BCAdsClientBanner graphicAdClient = new BCAdsClientBanner(101021,
				BCAdsClientBanner.ADTYPE_MWEB,
				BCAdsClientBanner.IMGSIZE_MWEB_216x36, this);
		ImageView graphicalAds = (ImageView) findViewById(R.id.ads2);
		graphicAdClient.getGraphicalAd(graphicalAds);
		
		BCAdsClientBanner graphicAdClient1 = new BCAdsClientBanner(106400,
				BCAdsClientBanner.ADTYPE_MWEB,
				BCAdsClientBanner.IMGSIZE_MWEB_216x36, this);
		ImageView graphicalAds1 = (ImageView) findViewById(R.id.ads3);
		graphicAdClient1.getGraphicalAd(graphicalAds1);
	}
	
	private void playMusic(String url){
		//play
		isPlay = true;
		Intent intent = new Intent(Intent.ACTION_VIEW); 
        intent.setDataAndType(Uri.parse(url), "audio/mp3"); 
        startActivity(intent); 
	}
	
	private void downloadMusic(String url, String fileName){    
            
			//download
			Uri Download_Uri = Uri.parse(url);
			DownloadManager.Request request = new DownloadManager.Request(Download_Uri);

//			request.setDestinationInExternalPublicDir(
//					Environment.DIRECTORY_DOWNLOADS + "/Doujin/",doujinname);
			request.setDestinationUri(Uri.fromFile(new File( Environment.getExternalStorageDirectory(),fileName+".mp3") ));

			long download_id = downloadManager.enqueue(request);

			// Save the download id
			Editor PrefEdit = preferenceManager.edit();
			PrefEdit.putLong(Download_ID, download_id);
			PrefEdit.commit();
			
			
			Toast.makeText(this,
					"Start download.",
					Toast.LENGTH_SHORT).show();
	}

	class ItemAdapter extends BaseAdapter {

		private class ViewHolder {
			public TextView text;
			public TextView desc;
			public Button btn_play;
			public Button btn_download;
		}

		
		public int getCount() {
			return MusicListActivity.array.size();
		}

		
		public Object getItem(int position) {
			return position;
		}

		
		public long getItemId(int position) {
			return position;
		}

		
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			View view = convertView;
			final ViewHolder holder;
			if (convertView == null) {
				view = getLayoutInflater().inflate(R.layout.youtube_item,
						parent, false);
				holder = new ViewHolder();
				holder.text = (TextView) view.findViewById(R.id.text);
				holder.desc = (TextView) view.findViewById(R.id.desc);
				holder.btn_play = (Button) view.findViewById(R.id.btn_play);
				holder.btn_download = (Button) view.findViewById(R.id.btn_download);
				view.setTag(holder);
			} else {
				holder = (ViewHolder) view.getTag();
			}

			try {
				String t = URLDecoder.decode(titles[position], "UTF-8");
				holder.text.setText(Html.fromHtml((position+1)+". "+t));
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				holder.text.setText(Html.fromHtml((position+1)+". "+titles[position]));
			}
			
			String descText = "";
			if(bitrates[position]!=null){
				descText += bitrates[position] + "    ";
			}
			
			if(durations[position]!=null){
				descText += durations[position] + "    ";
			}
			
			if(filesizes[position]!=null){
				descText += filesizes[position];
			}
			
			holder.desc.setText(Html.fromHtml(descText));
			
			holder.btn_play.setOnClickListener(new OnClickListener() {
				
				public void onClick(View v) {
					// TODO Auto-generated method stub
					playMusic(urls[position]);
				}
			});
			holder.btn_download.setOnClickListener(new OnClickListener() {
				
				public void onClick(View v) {
					// TODO Auto-generated method stub
					downloadMusic(urls[position], titles[position]);
				}
			});

			return view;
		}
	}
	
	
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		if(isPlay){
			isPlay = false;
			Intent i = new Intent(getApplicationContext(), AdsWrapper.class);
	        i.putExtra("partnerId", "101021");
	        i.putExtra("appId", "com.mazmellow.musicsearcher");
	        i.putExtra("showAt", "start");
	        i.putExtra("skipEarly", "true");
	        i.putExtra("adsTimeout", "10");
	        startActivity(i);
		}
		

		//download
//		IntentFilter intentFilter 
//			= new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
//		registerReceiver(downloadReceiver, intentFilter);
	}
	
	
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		//unregisterReceiver(downloadReceiver);
	}

	private BroadcastReceiver downloadReceiver = new BroadcastReceiver() {

		
		public void onReceive(Context arg0, Intent arg1) {
			CheckDwnloadStatus();
			// TODO Auto-generated method stub
			DownloadManager.Query query = new DownloadManager.Query();
			query.setFilterById(preferenceManager.getLong(strPref_Download_ID, 0));
			Cursor cursor = downloadManager.query(query);
			if(cursor.moveToFirst()){
				int columnIndex = cursor.getColumnIndex(DownloadManager.COLUMN_STATUS);
				int status = cursor.getInt(columnIndex);
				if(status == DownloadManager.STATUS_SUCCESSFUL){

					//Retrieve the saved request id
					long downloadID = preferenceManager.getLong(strPref_Download_ID, 0);

					ParcelFileDescriptor file;
					try {
						file = downloadManager.openDownloadedFile(downloadID);
						Toast.makeText(MusicListActivity.this,
								"File Downloaded: " + file.toString(),
								Toast.LENGTH_LONG).show();
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						Toast.makeText(MusicListActivity.this,
								e.toString(), Toast.LENGTH_LONG).show();
					}
				}
			}
		}	
	};
	private void CheckDwnloadStatus(){

		// TODO Auto-generated method stub
		DownloadManager.Query query = new DownloadManager.Query();
		long id = preferenceManager.getLong(strPref_Download_ID, 0);
		query.setFilterById(id);
		Cursor cursor = downloadManager.query(query);
		if(cursor.moveToFirst()){
			int columnIndex = cursor.getColumnIndex(DownloadManager.COLUMN_STATUS);
			int status = cursor.getInt(columnIndex);
			int columnReason = cursor.getColumnIndex(DownloadManager.COLUMN_REASON);
			int reason = cursor.getInt(columnReason);

			switch(status){
				case DownloadManager.STATUS_FAILED:
				String failedReason = "";
				switch(reason){
					case DownloadManager.ERROR_CANNOT_RESUME:
					failedReason = "ERROR_CANNOT_RESUME";
					break;
					case DownloadManager.ERROR_DEVICE_NOT_FOUND:
					failedReason = "ERROR_DEVICE_NOT_FOUND";
					break;
					case DownloadManager.ERROR_FILE_ALREADY_EXISTS:
					failedReason = "ERROR_FILE_ALREADY_EXISTS";
					break;
					case DownloadManager.ERROR_FILE_ERROR:
					failedReason = "ERROR_FILE_ERROR";
					break;
					case DownloadManager.ERROR_HTTP_DATA_ERROR:
					failedReason = "ERROR_HTTP_DATA_ERROR";
					break;
					case DownloadManager.ERROR_INSUFFICIENT_SPACE:
					failedReason = "ERROR_INSUFFICIENT_SPACE";
					break;
					case DownloadManager.ERROR_TOO_MANY_REDIRECTS:
					failedReason = "ERROR_TOO_MANY_REDIRECTS";
					break;
					case DownloadManager.ERROR_UNHANDLED_HTTP_CODE:
					failedReason = "ERROR_UNHANDLED_HTTP_CODE";
					break;
					case DownloadManager.ERROR_UNKNOWN:
					failedReason = "ERROR_UNKNOWN";
					break;
				}

				Toast.makeText(MusicListActivity.this,
					"FAILED: " + failedReason,
					Toast.LENGTH_LONG).show();
				break;
				case DownloadManager.STATUS_PAUSED:
				String pausedReason = "";

				switch(reason){
					case DownloadManager.PAUSED_QUEUED_FOR_WIFI:
					pausedReason = "PAUSED_QUEUED_FOR_WIFI";
					break;
					case DownloadManager.PAUSED_UNKNOWN:
					pausedReason = "PAUSED_UNKNOWN";
					break;
					case DownloadManager.PAUSED_WAITING_FOR_NETWORK:
					pausedReason = "PAUSED_WAITING_FOR_NETWORK";
					break;
					case DownloadManager.PAUSED_WAITING_TO_RETRY:
					pausedReason = "PAUSED_WAITING_TO_RETRY";
					break;
				}

				Toast.makeText(MusicListActivity.this,
					"PAUSED: " + pausedReason,
					Toast.LENGTH_LONG).show();
				break;
				case DownloadManager.STATUS_PENDING:
				Toast.makeText(MusicListActivity.this,
					"PENDING",
					Toast.LENGTH_LONG).show();
				break;
				case DownloadManager.STATUS_RUNNING:
				Toast.makeText(MusicListActivity.this,
					"RUNNING",
					Toast.LENGTH_LONG).show();
				break;
				case DownloadManager.STATUS_SUCCESSFUL:

				Toast.makeText(MusicListActivity.this,
					"SUCCESSFUL",
					Toast.LENGTH_LONG).show();
				//GetFile();
				break;
			}
		}
	}
}