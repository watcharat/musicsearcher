package com.mazmellow.musicsearcher;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import buzzcity.android.sdk.BCAdsClientBanner;


import com.mazmellow.musicsearcher.RequestHttpClient.RequestHttpClientListenner;

public class MainActivity extends Activity implements
		RequestHttpClientListenner {

	EditText editText;
	RequestHttpClient httpClient;
	ListView listView;
	ArrayList<String> historyList;
	DatabaseManager myDb;
	ItemAdapter adapter;

	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		editText = (EditText) findViewById(R.id.editText1);

		listView = (ListView) findViewById(android.R.id.list);
		adapter = new ItemAdapter();
		((ListView) listView).setAdapter(adapter);
		if (listView != null) {
			listView.setOnItemClickListener(new OnItemClickListener() {
				
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					String keyword = historyList.get(position);
					searchMusic(keyword);
				}
			});
		}

		myDb = new DatabaseManager(this);
		historyList = myDb.getAllHistory();
		if (historyList == null) {
			historyList = new ArrayList<String>();
		}

//		BCAdsClientBanner graphicAdClient = new BCAdsClientBanner(101021,
//				BCAdsClientBanner.ADTYPE_MWEB,
//				BCAdsClientBanner.IMGSIZE_MWEB_216x36, this);
//		ImageView graphicalAds = (ImageView) findViewById(R.id.ads0);
//		graphicAdClient.getGraphicalAd(graphicalAds);

		BCAdsClientBanner graphicAdClient1 = new BCAdsClientBanner(106400,
				BCAdsClientBanner.ADTYPE_MWEB,
				BCAdsClientBanner.IMGSIZE_MWEB_216x36, this);
		ImageView graphicalAds1 = (ImageView) findViewById(R.id.ads1);
		graphicAdClient1.getGraphicalAd(graphicalAds1);
	}
	
	public void onClickAppList(View view){
		Intent intent = new Intent(this, AppListActivity.class);
		startActivity(intent);
	}

	class ItemAdapter extends BaseAdapter {

		private class ViewHolder {
			public TextView text;
		}

		
		public int getCount() {
			if (historyList != null) {
				return historyList.size();
			} else {
				return 0;
			}
		}

		
		public Object getItem(int position) {
			return position;
		}

		
		public long getItemId(int position) {
			return position;
		}

		
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			View view = convertView;
			final ViewHolder holder;
			if (convertView == null) {
				view = getLayoutInflater().inflate(R.layout.history_item,
						parent, false);
				holder = new ViewHolder();
				holder.text = (TextView) view.findViewById(R.id.text);
				view.setTag(holder);
			} else {
				holder = (ViewHolder) view.getTag();
			}

			holder.text.setText(historyList.get(position));

			return view;
		}
	}

	public void onClickSearchMusic(View view) {
		System.out.println("onClickSearchMusic");
		if (editText != null) {
			Editable editable = editText.getEditableText();
			String word = editable.toString();
			System.out.println("word = " + word);
			if (word != null && !word.equals("")) {
				searchMusic(word);
			}
		}

	}

	public void searchMusic(String title) {
		// search
		myDb.insertHistory(title);
		historyList = myDb.getAllHistory();
		adapter.notifyDataSetChanged();
		try {
			String url = "http://mp3skull.com/mp3/"
					+ URLDecoder.decode(title, "UTF-8") + ".html";
			httpClient = new RequestHttpClient(url, MainActivity.this,
					MainActivity.this);
			httpClient.start();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	
	public void onRequestStringCallback(String response) {
		// TODO Auto-generated method stub
		// System.out.println("response = "+response);
		ArrayList<MusicObject> arrayList = new ArrayList<MusicObject>();
		while (response.indexOf("song_html") != -1) {
			response = response.substring(response.indexOf("song_html"));
			MusicObject musicObject = new MusicObject();

			String startIndex = "<!-- info mp3 here -->";
			String endIndex = "</div>";
			int countBack = 0;
			if (response.indexOf(startIndex) != -1
					&& response.indexOf(endIndex) != -1) {
				String metadata = response.substring(
						response.indexOf(startIndex) + startIndex.length(),
						response.indexOf(endIndex) - countBack);

				// check meta data
				System.out.println("metadata = " + metadata);
				if (metadata.indexOf("<br />") != -1) {
					String[] metas = metadata.split("<br />");
					for (int i = 0; i < metas.length; i++) {
						String meta = metas[i];
						if (meta.indexOf("kbps") != -1) {
							musicObject.setBitrate(meta.trim());
							continue;
						} else if (meta.indexOf(":") != -1) {
							musicObject.setDuration(meta.trim());
							continue;
						} else if (meta.indexOf("mb") != -1) {
							musicObject.setFilesize(meta.trim());
							continue;
						}
					}
				}

				if (response.indexOf("right_song") != -1) {
					response = response.substring(response
							.indexOf("right_song"));
					startIndex = "\"font-size:15px;\"><b>";
					endIndex = "</b></div>";
					countBack = 0;
					String title = response.substring(
							response.indexOf(startIndex) + startIndex.length(),
							response.indexOf(endIndex) - countBack);
					musicObject.setTitle(title);

					response = response.substring(response.indexOf(endIndex)
							+ endIndex.length());
					startIndex = "href=\"";
					endIndex = "\" rel";
					countBack = 0;
					String url = response.substring(
							response.indexOf(startIndex) + startIndex.length(),
							response.indexOf(endIndex) - countBack);
					musicObject.setUrl(url);

					arrayList.add(musicObject);
				}
			}

		}

		if (arrayList.size() > 0) {
			MusicListActivity.array = arrayList;
			Intent intent = new Intent(this, MusicListActivity.class);
			startActivity(intent);
		} else {
			// popup not found
			new AlertDialog.Builder(this)
					.setTitle("Not Found!")
					.setMessage(
							"Sorry we don't found any song. Please try again.")
					.setPositiveButton("OK",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {

								}
							});
		}
	}

}
